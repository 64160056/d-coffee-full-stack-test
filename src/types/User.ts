export default interface User {
    id?: number;
    name: string;
    tel: string;
    email: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }