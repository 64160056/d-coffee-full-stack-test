import type User from "@/types/User";
import http from "./axios";

function getUser() {
  return http.get("/Users");
}
function saveUser(User: User) {
  return http.post("/Users", User);
}
function updateUser(id: number, User: User) {
  return http.patch(`/Users/${id}`, User);
}
function deleteUser(id: number) {
  return http.delete(`/Users/${id}`);
}
function getUserName() {
  return http.get("/users/all");
}
export default { getUser, saveUser, updateUser, deleteUser ,getUserName};