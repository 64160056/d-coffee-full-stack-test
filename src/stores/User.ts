import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type User from '@/types/User'
import UserService from '@/services/user'
interface Name {
    Name:string
}
export const useUserStore = defineStore('User', () => {
    const User = ref<User[]>([]);
    const allUserName = ref<Name[]>([]);
    
    async function GetUser(){
        try {
            const res = await UserService.getUser();
            User.value = res.data
        }catch(e){
            console.log(e);
        }
    }
    async function GetUserName(){
        try {
            const res = await UserService.getUserName();
            allUserName.value = res.data
            console.log(res)
        }catch(e){
            console.log(e);
        }
    }

  return { GetUser,User,GetUserName,allUserName }
})